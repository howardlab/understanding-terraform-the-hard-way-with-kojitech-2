# understanding-terraform-the-hard-way-with-kojitech-2

terraform init
terraform validate - checks and validates the syntax
terraform plan - shows the desired state of the infra
terraform apply - creates the resource - commits the plan
terraform destroy
terraform refresh
terraform output