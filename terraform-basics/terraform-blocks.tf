
# Terraform Settings Block - this blockis meant to set constraint on terraform version
terraform {
  required_version = ">=1.1.0"

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 5.0"
      #version = "~> 3.21" # Optional but recommended in production
    }
  }
}

#### Provider block - Authentication and Authorisation
## - Method 1 using credentials - security keys exposed and hard coding
#provider "aws" {
#  region     = "us-west-2"
#  access_key = "my-access-key"
#  secret_key = "my-secret-key"
#}

## - Method 2 - using environment variables
# provider "aws" {}
# Run this section in terminal
# export AWS_ACCESS_KEY_ID="anaccesskey"
# export AWS_SECRET_ACCESS_KEY="asecretkey"
# export AWS_REGION="us-west-2"
# challenge of running this multiple times or in a team where we have got multiple users

# Method 3 - assume role 

# Method 4 - Assume role with web identity 

# Method 5 - Using profile 
provider "aws" {
  region  = "us-east-1"
  profile = "default"
}

# Resource Block - Bring into existence 
resource "aws_vpc" "main-kojitech" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "koji-tech-main"
  }
}

# reference a variable using var.the variable name

# Creating EC2
         #local name of the resource #resource name 
resource "aws_instance" "koji-foo_ec2" {
  ami           = var.ami_id      #reference a variable by using var.variable name. var is a function
  instance_type = var.instance_type

    tags = {
      Name = "koji-foo_ec2"
    }
}

######## output Block