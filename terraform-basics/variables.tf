# creating a variables block 

variable "ami_id"{
        type = string
        description = "AMI id "
        default  = "ami-08a52ddb321b32a8c"
}

variable "instance_type" {
    type = string 
    description = "instance type"
    default = "t2.micro"
  
}